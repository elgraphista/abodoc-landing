/*==============================================================*/
// Raque Contact Form  JS
/*==============================================================*/
(function ($) {
    "use strict"; // Start of use strict
    $("#contactForm").validator().on("submit", function (event) {
        if (event.isDefaultPrevented()) {
            // handle the invalid form...
            formError();      
            submitMSG(false, "El formulario tiene campos sin completar o con errores");
        } else {
            // everything looks good!
            event.preventDefault();
            submitForm();
        }
    });

    $("#convenioForm").validator().on("submit", function (event) {
        if (event.isDefaultPrevented()) {
            // handle the invalid form...
            formError();      
            submitMSG(false, "El formulario tiene campos sin completar o con errores");
        } else {
            // everything looks good!
            event.preventDefault();
            submitForm();
        }
    });


    function submitForm(){
        // Initiate Variables With Form Content
        var name = $("#nombre").val();
        var email = $("#email").val();
        var phone_number = $("#telefono").val();
        var message = $("#mensaje").val();


        $.ajax({
            type: "POST",
            url: "/assets/php/form-process.php",
            data: "name=" + name + "&email=" + email + "&phone_number=" + phone_number + "&message=" + message,
            success : function(text){
                if (text == "success"){
                    formSuccess();
                } else {
                    formError();
                    submitMSG(false,text);
                }
            }
        });
    }

    function formSuccess(){
        $("#contactForm")[0].reset();
        submitMSG(true, "Mensaje enviado!")
    }

    function formError(){
        $("#contactForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
            $(this).removeClass();
        });
    }

    function submitMSG(valid, msg){
        if(valid){
            var msgClasses = "h4 tada animated";
        } else {
            var msgClasses = "h4  text-warning";
        }
        $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
    }
}(jQuery)); // End of use strict